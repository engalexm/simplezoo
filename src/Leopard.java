import java.util.ArrayList;

public class Leopard extends Animal {
    public static final Diet diet = Diet.CARNIVORE;
    public static final Food[] food = new Food[] {Food.MEAT};
    public static final int temperature = 55;
    public static final int litter = 4;

    public Leopard(Gender gender) {
        super(gender);
    }

    public ArrayList<Animal> giveBirth() throws WrongGenderException {
        if(this.getGender() == Gender.MALE) {
            throw new WrongGenderException("giveBirth() ERROR: Male cannot give birth!");
        } else {
            ArrayList<Animal> result = new ArrayList<Animal>();
            for(int i = 0; i < Leopard.litter; i++) if(Math.random() <= 0.1) result.add(Math.random() <= 0.5 ? new Leopard(Gender.MALE) : new Leopard(Gender.FEMALE));
            return result;
        }
    }
}