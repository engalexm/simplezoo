import java.util.ArrayList;

public class Warthog extends Animal {
    public static final Diet diet = Diet.OMNIVORE;
    public static final Food[] food = new Food[] {Food.GRASS, Food.EGGS, Food.FRUIT};
    public static final int temperature = 70;
    public static final int litter = 8;

    public Warthog(Gender gender) {
        super(gender);
    }

    public ArrayList<Animal> giveBirth() throws WrongGenderException {
        if(this.getGender() == Gender.MALE) {
            throw new WrongGenderException("giveBirth() ERROR: Male cannot give birth!");
        } else {
        ArrayList<Animal> result = new ArrayList<Animal>();
        for(int i = 0; i < Warthog.litter; i++) if(Math.random() <= 0.1) result.add(Math.random() <= 0.5 ? new Warthog(Gender.MALE) : new Warthog(Gender.FEMALE));
        return result;
        }
    }
  
}