import java.util.ArrayList;

public class Meerkat extends Animal {
    public static final Diet diet = Diet.OMNIVORE;
    public static final Food[] food = new Food[] {Food.BUGS, Food.GRASS, Food.EGGS};
    public static final int temperature = 60;
    public static final int litter = 8;

    public Meerkat(Gender gender) {
        super(gender);
    }

    public ArrayList<Animal> giveBirth() throws WrongGenderException {
        if(this.getGender() == Gender.MALE) {
            throw new WrongGenderException("giveBirth() ERROR: Male cannot give birth!");
        } else {
            ArrayList<Animal> result = new ArrayList<Animal>();
            for(int i = 0; i < Meerkat.litter; i++) if(Math.random() <= 0.1) result.add(Math.random() <= 0.5 ? new Meerkat(Gender.MALE) : new Meerkat(Gender.FEMALE));
            return result;
        }
    }
}