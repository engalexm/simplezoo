import java.util.ArrayList;

public class SimpleZoo {
     public static void main(String[] args) throws InterruptedException {
       
        // Create each animal type's enclosures with Eve & Adam, then set limits
        Enclosure<Warthog> warthogs = new Enclosure<Warthog>();
        warthogs.setMaximumSize(7);
        warthogs.addAnimal(new Warthog(Gender.FEMALE));
        warthogs.addAnimal(new Warthog(Gender.MALE));
        
        Enclosure<Kangaroo> kangaroos = new Enclosure<Kangaroo>();
        kangaroos.setMaximumSize(5);
        kangaroos.addAnimal(new Kangaroo(Gender.FEMALE));
        kangaroos.addAnimal(new Kangaroo(Gender.MALE));
        
        Enclosure<Meerkat> meerkats = new Enclosure<Meerkat>();
        meerkats.setMaximumSize(100);
        meerkats.addAnimal(new Meerkat(Gender.FEMALE));
        meerkats.addAnimal(new Meerkat(Gender.MALE));

        Enclosure<Leopard> leopards = new Enclosure<Leopard>();
        leopards.setMaximumSize(3);
        leopards.addAnimal(new Leopard(Gender.FEMALE));
        leopards.addAnimal(new Leopard(Gender.MALE));
        
        Enclosure<Bear> bears = new Enclosure<Bear>();
        bears.setMaximumSize(10);
        bears.addAnimal(new Bear(Gender.FEMALE));
        bears.addAnimal(new Bear(Gender.MALE));

        Enclosure<Zebra> zebras = new Enclosure<Zebra>();
        zebras.addAnimal(new Zebra(Gender.FEMALE));
        zebras.addAnimal(new Zebra(Gender.MALE));
        zebras.setMaximumSize(5);

        Enclosure<Gorilla> gorillas = new Enclosure<Gorilla>();
        gorillas.setMaximumSize(7);
        gorillas.addAnimal(new Gorilla(Gender.FEMALE));
        gorillas.addAnimal(new Gorilla(Gender.MALE));

        Enclosure<Elephant> elephants = new Enclosure<Elephant>();
        elephants.setMaximumSize(3);
        elephants.addAnimal(new Elephant(Gender.FEMALE));
        elephants.addAnimal(new Elephant(Gender.MALE));

        int iteration = 0;

        // Initiate main breeding/death loop
        while(!((warthogs.getAtCapacity() == true) && (kangaroos.getAtCapacity() == true)
                && (meerkats.getAtCapacity() == true) && (leopards.getAtCapacity() == true)
                && (bears.getAtCapacity() == true) && (zebras.getAtCapacity() == true)
                && (gorillas.getAtCapacity() == true) && (elephants.getAtCapacity() == true))) {

            System.out.println("\n\n========= ITERATION " + iteration);

            // Per enclosure, stage death wave and birth wave for survivors, execute, then print results
            ArrayList<Animal> toKill = new ArrayList<Animal>(); ArrayList<Animal> toBirth = new ArrayList<Animal>();

            // Warthogs
            if(warthogs.getAnimals().size() != 0) {
                for(Warthog w : warthogs.getAnimals()) {
                    if(!w.heartbeat()) {
                        toKill.add(w);
                    } else {
                        try {
                            toBirth.addAll(w.giveBirth());
                        } catch (WrongGenderException wge) {
                            System.out.println(wge);
                        }
                    }
                }
                // Commence executions
                for(Animal d : toKill) {
                    warthogs.removeAnimal((Warthog) d);
                }
                // Commence births
                for(Animal b : toBirth) {
                    warthogs.addAnimal((Warthog) b);
                }
                System.out.println("Warthog population: " + warthogs.getAnimals().size() + " / " + warthogs.getMaximumSize());
                System.out.println(toKill.size() + " died, " + toBirth.size() + " born!\n"); 
            } else {
                System.out.println("Oops! Warthogs went extinct!\n");
            }
            
            // Kangaroos
            if(kangaroos.getAnimals().size() != 0) {
                toKill = new ArrayList<Animal>(); toBirth = new ArrayList<Animal>();
                for(Kangaroo k : kangaroos.getAnimals()) {
                    if(!k.heartbeat()) {
                        toKill.add(k);
                    } else {
                        try {
                            toBirth.addAll(k.giveBirth());
                        } catch (WrongGenderException wge) {
                            System.out.println(wge);
                        }
                    }
                }
                for(Animal d : toKill) {
                    kangaroos.removeAnimal((Kangaroo) d);
                }
                for(Animal b : toBirth) {
                    kangaroos.addAnimal((Kangaroo) b);
                }
                System.out.println("Kangaroo population: " + kangaroos.getAnimals().size() + " / " + kangaroos.getMaximumSize());
                System.out.println(toKill.size() + " died, " + toBirth.size() + " born!\n");
            } else {
                System.out.println("Oops! Kangaroos went extinct!\n");
            }
            
            // Meerkats
            if(meerkats.getAnimals().size() != 0) {
                toKill = new ArrayList<Animal>(); toBirth = new ArrayList<Animal>();
                for(Meerkat m : meerkats.getAnimals()) {
                    if(!m.heartbeat()) {
                        toKill.add(m);
                    } else {
                        try {
                            toBirth.addAll(m.giveBirth());
                        } catch (WrongGenderException wge) {
                            System.out.println(wge);
                        }
                    }
                }
                for(Animal d : toKill) {
                    meerkats.removeAnimal((Meerkat) d);
                }
                for(Animal b : toBirth) {
                    meerkats.addAnimal((Meerkat) b);
                }
                System.out.println("Meerkat population: " + meerkats.getAnimals().size() + " / " + meerkats.getMaximumSize());
                System.out.println(toKill.size() + " died, " + toBirth.size() + " born!\n");
            } else {
                System.out.println("Oops! Meerkats went extinct!\n");
            }

            // Leopards
            if(leopards.getAnimals().size() != 0) {
                toKill = new ArrayList<Animal>(); toBirth = new ArrayList<Animal>();
                for(Leopard l : leopards.getAnimals()) {
                    if(!l.heartbeat()) {
                        toKill.add(l);
                    } else {
                        try {
                            toBirth.addAll(l.giveBirth());
                        } catch (WrongGenderException wge) {
                            System.out.println(wge);
                        }
                    }
                }
                for(Animal d : toKill) {
                    leopards.removeAnimal((Leopard) d);
                }
                for(Animal b : toBirth) {
                    leopards.addAnimal((Leopard) b);
                }
                System.out.println("Leopard population: " + leopards.getAnimals().size() + " / " + leopards.getMaximumSize());
                System.out.println(toKill.size() + " died, " + toBirth.size() + " born!\n");
            } else {
                System.out.println("Oops! Leopards went extinct!\n");
            }
           
            // Bears
            if(bears.getAnimals().size() != 0) {
                toKill = new ArrayList<Animal>(); toBirth = new ArrayList<Animal>();
                for(Bear b : bears.getAnimals()) {
                    if(!b.heartbeat()) {
                        toKill.add(b);
                    } else {
                        try {
                            toBirth.addAll(b.giveBirth());
                        } catch (WrongGenderException wge) {
                            System.out.println(wge);
                        }
                    }
                }
                for(Animal d : toKill) {
                    bears.removeAnimal((Bear) d);
                }
                for(Animal b : toBirth) {
                    bears.addAnimal((Bear) b);
                }
                System.out.println("Bear population: " + bears.getAnimals().size() + " / " + bears.getMaximumSize());
                System.out.println(toKill.size() + " died, " + toBirth.size() + " born!\n");
            } else {
                System.out.println("Oops! Bears went extinct!\n");
            }
            
            // Zebras
            if(zebras.getAnimals().size() != 0) {
                toKill = new ArrayList<Animal>(); toBirth = new ArrayList<Animal>();
                for(Zebra z : zebras.getAnimals()) {
                    if(!z.heartbeat()) {
                        toKill.add(z);
                    } else {
                        try {
                            toBirth.addAll(z.giveBirth());
                        } catch (WrongGenderException wge) {
                            System.out.println(wge);
                        }
                    }
                }
                for(Animal d : toKill) {
                    zebras.removeAnimal((Zebra) d);
                }
                for(Animal b : toBirth) {
                    zebras.addAnimal((Zebra) b);
                }
                System.out.println("Zebra population: " + zebras.getAnimals().size() + " / " + zebras.getMaximumSize());
                System.out.println(toKill.size() + " died, " + toBirth.size() + " born!\n");
            } else {
                System.out.println("Oops! Zebras went extinct!\n");
            }
            
            // Gorillas
            if(gorillas.getAnimals().size() != 0) {
                toKill = new ArrayList<Animal>(); toBirth = new ArrayList<Animal>();
                for(Gorilla g : gorillas.getAnimals()) {
                    if(!g.heartbeat()) {
                        toKill.add(g);
                    } else {
                        try {
                            toBirth.addAll(g.giveBirth());
                        } catch (WrongGenderException wge) {
                            System.out.println(wge);
                        }
                    }
                }
                for(Animal d : toKill) {
                    gorillas.removeAnimal((Gorilla) d);
                }
                for(Animal b : toBirth) {
                    gorillas.addAnimal((Gorilla) b);
                }
                System.out.println("Gorilla population: " + gorillas.getAnimals().size() + " / " + gorillas.getMaximumSize());
                System.out.println(toKill.size() + " died, " + toBirth.size() + " born!\n");
            } else {
                System.out.println("Oops! Gorillas went extinct!\n");
            }
            
            // Elephants
            if(elephants.getAnimals().size() != 0) {
                toKill = new ArrayList<Animal>(); toBirth = new ArrayList<Animal>();
                for(Elephant e : elephants.getAnimals()) {
                    if(!e.heartbeat()) {
                        toKill.add(e);
                    } else {
                        try {
                            toBirth.addAll(e.giveBirth());
                        } catch (WrongGenderException wge) {
                            System.out.println(wge);
                        }
                    }
                }
                for(Animal d : toKill) {
                    elephants.removeAnimal((Elephant) d);
                }
                for(Animal b : toBirth) {
                    elephants.addAnimal((Elephant) b);
                }
                System.out.println("Elephant population: " + elephants.getAnimals().size() + " / " + elephants.getMaximumSize());
                System.out.println(toKill.size() + " died, " + toBirth.size() + " born!\n");
            } else {
                System.out.println("Oops! Elephants went extinct!\n");
            }

            Thread.sleep(1000);
            iteration++;
            
            if(warthogs.getAnimals().size()==0 && kangaroos.getAnimals().size()==0 && meerkats.getAnimals().size()==0
                && leopards.getAnimals().size()==0 && bears.getAnimals().size()==0 && zebras.getAnimals().size()==0
                && gorillas.getAnimals().size()==0 && elephants.getAnimals().size()==0) {
                    System.out.println("\n\n*************** ALL ANIMALS HAVE BECOME EXTINCT! ***************");
                    break;
            }

        }



    }
}