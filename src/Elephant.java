import java.util.ArrayList;

public class Elephant extends Animal {
    public static final Diet diet = Diet.HERBIVORE;
    public static final Food[] food = new Food[] {Food.GRASS, Food.FRUIT};
    public static final int temperature = 55;
    public static final int litter = 1;

    public Elephant(Gender gender) {
        super(gender);
    }

    public ArrayList<Animal> giveBirth() throws WrongGenderException {
        if(this.getGender() == Gender.MALE) {
            throw new WrongGenderException("giveBirth() ERROR: Male cannot give birth!");
        } else {
            ArrayList<Animal> result = new ArrayList<Animal>();
            for(int i = 0; i < Elephant.litter; i++) if(Math.random() <= 0.1) result.add(Math.random() <= 0.5 ? new Elephant(Gender.MALE) : new Elephant(Gender.FEMALE));
            return result;
        }
    }
}