import java.util.ArrayList;

public class Bear extends Animal {
    public static final Diet diet = Diet.CARNIVORE;
    public static final Food[] food = new Food[] {Food.MEAT, Food.FISH};
    public static final int temperature = 58;
    public static final int litter = 2;

    public Bear(Gender gender) {
        super(gender);
    }

    public ArrayList<Animal> giveBirth() throws WrongGenderException {
        if(this.getGender() == Gender.MALE) {
            throw new WrongGenderException("giveBirth() ERROR: Male cannot give birth!");
        } else {
            ArrayList<Animal> result = new ArrayList<Animal>();
            for(int i = 0; i < Bear.litter; i++) if(Math.random() <= 0.1) result.add(Math.random() <= 0.5 ? new Bear(Gender.MALE) : new Bear(Gender.FEMALE));
            return result;
        }
    }
}