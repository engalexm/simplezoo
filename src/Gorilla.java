import java.util.ArrayList;

public class Gorilla extends Animal {
    public static final Diet diet = Diet.HERBIVORE;
    public static final Food[] food = new Food[] {Food.GRASS, Food.FRUIT};
    public static final int temperature = 83;
    public static final int litter = 1;

    public Gorilla(Gender gender) {
        super(gender);
    }

    public ArrayList<Animal> giveBirth() throws WrongGenderException {
        if(this.getGender() == Gender.MALE) {
            throw new WrongGenderException("giveBirth() ERROR: Male cannot give birth!");
        } else {
            ArrayList<Animal> result = new ArrayList<Animal>();
            for(int i = 0; i < Gorilla.litter; i++) if(Math.random() <= 0.1) result.add(Math.random() <= 0.5 ? new Gorilla(Gender.MALE) : new Gorilla(Gender.FEMALE));
            return result;
        }
    }
}