import java.util.ArrayList;

public class Enclosure<T> {
    private ArrayList<T> animals;
    private int maximumSize;
    private boolean atCapacity = false;

    public Enclosure() {
        this.animals = new ArrayList<T>();
    }

    public ArrayList<T> getAnimals() {
        return this.animals;
    }

    public void setMaximumSize(int maximumSize) {
        this.maximumSize = maximumSize;
    }

    public int getMaximumSize() {
        return this.maximumSize;
    }

    public boolean getAtCapacity() {
        return this.atCapacity;
    }

    // Set atCapacity flag to TRUE if at or over capacity
    public void addAnimal(T animal) {
        if(this.animals.size() < this.maximumSize) {
            this.animals.add(animal);
            if(this.animals.size() == this.maximumSize) this.atCapacity = true;
        } else {
            this.atCapacity = true;
        }
    }

    // Will do removal by animalID defined in Animal class
    public void removeAnimal(T animal) {
        this.animals.remove(animal);
    }



}