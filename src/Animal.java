import java.util.ArrayList;
import java.util.UUID;

public abstract class Animal {
    private String animalID = UUID.randomUUID().toString();
    private Gender gender;

    public Animal(Gender gender) {
        this.gender = gender;
    }

    public String getAnimalID() {
        return this.animalID;
    }

    public Gender getGender() {
        return this.gender;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Animal)) return false;
        Animal other = (Animal) o;
        return this.getAnimalID().equals(other.getAnimalID());
    }

    public abstract ArrayList<Animal> giveBirth() throws WrongGenderException;

    public boolean heartbeat() {
        return Math.random() <= 0.8 ? true : false;
    }

}