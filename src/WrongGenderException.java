public class WrongGenderException extends Exception {
    // Required for serialization bc I guess Exception is Serializable
    private static final long serialVersionUID = 1L;

    public WrongGenderException(String errorMessage) {
        super(errorMessage);
    }
}